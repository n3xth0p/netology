import os
import requests


class YaUploader:
    def __init__(self, token: str):
        self.token = token
        self.HEADERS = {
            "Authorization": f"OAuth {self.token}"
        }
        self.URL = "https://cloud-api.yandex.net/v1/disk/resources"

    def upload(self, local_file_path: str, upload_directory='netology_upload'):
        """Метод загруджает файл file_path на яндекс диск"""
        if os.path.exists(local_file_path):
            filename = local_file_path.split("\\")[-1]
        else:
            raise Exception

        if not self.__exists(upload_directory):
            self.create_dir(upload_directory)
        elif self.__exists(upload_directory) is True and self.__exists(f"{upload_directory}/{filename}") is False:
            pass
        else:
            self.__remove_file(f"{upload_directory}/{filename}")

        result = self.__upload_file(local_file_path, f"{upload_directory}/{filename}")

        return result

    def __upload_file(self, local_file_path, upload_filepath):
        # шаг 1. получение URL для загрузки файла
        response = requests.get(
            f"{self.URL}/upload",
            params={
                "path": upload_filepath
            },
            headers=self.HEADERS
        )

        href = response.json()["href"]

        # шаг 2. загрузка тела файла
        with open(local_file_path, "rb") as f:
            upload_response = requests.put(href, files={"file": f})
            upload_response.raise_for_status()

        return upload_response.status_code

    def __exists(self, name: str):
        response = requests.get(
            self.URL,
            params={
                "path": name
            },
            headers=self.HEADERS
        )

        if response.status_code == 404:
            return False
        elif response.status_code == 200:
            return True
        else:
            raise Exception

    def __remove_file(self, file_path):
        response = requests.delete(
            self.URL,
            params={
                "path": file_path
            },
            headers=self.HEADERS
        )
        if response.status_code in [200, 202, 204]:
            return True
        else:
            raise Exception

    def create_dir(self, path):
        response = requests.put(
            self.URL,
            params={
                "path": path,
                "templated": "true"
            },
            headers=self.HEADERS
        )
        if response.status_code == 201:
            return True
        else:
            raise Exception


if __name__ == '__main__':
    FILENAME_PATH = 'c:\\TEMP\\upload_test01.txt'
    DIRECTORY_UPLOAD = 'test_ya_disk_directory'
    uploader = YaUploader('')

    print(f"Код загрузки файла - {uploader.upload(FILENAME_PATH, DIRECTORY_UPLOAD)}")
