import requests

if __name__ == "__main__":
    TOKEN = ""

    heroes_list = ["Hulk", "Captain America", "Thanos"]
    search_hero_data_url = f"https://superheroapi.com/api/{TOKEN}/search/"

    hero_data = []

    for hero in heroes_list:
        response = requests.get(search_hero_data_url + hero)
        response.raise_for_status()

        finded_hero_list = response.json()['results']

        if str(finded_hero_list[0]['name']).lower() == hero.lower():
            temp_dict = {}
            temp_dict['name'] = finded_hero_list[0]['name']
            temp_dict['id'] = finded_hero_list[0]['id']
            temp_dict['intelligence'] = finded_hero_list[0]['powerstats']['intelligence']
            hero_data.append(temp_dict)

    sort_intelligence_list = list(sorted(hero_data, key=lambda x: int(x['intelligence']), reverse=True))
    print(
        f"Самый умный супергерой {sort_intelligence_list[0]['name']}, "
        f"с интеллектом: {sort_intelligence_list[0]['intelligence']}")
