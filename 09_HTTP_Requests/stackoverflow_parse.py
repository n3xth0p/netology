import requests
import time
import pprint as pp

if __name__ == "__main__":
    now_time = int(time.time())
    two_days_ago_time = now_time - 2 * 86400

    tag = "Python"
    response = requests.get(
        "https://api.stackexchange.com/2.2/questions",
        params={
            "site": "stackoverflow",
            "fromdate": two_days_ago_time,
            "todate": now_time,
            "order": "desc",
            "sort": "activity",
            "tagged": tag,
            "filter": "default"
        }
    )
    response.raise_for_status()

    title_list = []

    for item in response.json()['items']:
        temp_list = [item['title'], item['tags']]

        title_list.append(temp_list)

    print(f"Вопросы с тегом {tag} на stackoverflow за последние 48 часов \n")
    for counter, value in enumerate(title_list):
        print(f"{counter + 1}. {value[0]}, теги - {value[1]}")
