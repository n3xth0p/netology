import json
import os
import xml.etree.ElementTree as ET


def get_filtered_words(filename_path, format, min_word_lenght):
    if not os.path.exists(filename_path):
        return -1
    filtered_words = []
    if format == "json":
        with open(filename_path, "r", encoding="utf-8") as f:
            data = json.load(f)
            for one_item in data['rss']['channel']['items']:
                words = one_item['description'].lower().strip().split()
                for one_word in words:
                    if len(one_word) >= min_word_lenght:
                        filtered_words.append(one_word)
        return filtered_words

    elif format == "xml":
        parser = ET.XMLParser(encoding="utf-8")
        xml_tree = ET.parse(filename_path, parser)
        xml_root = xml_tree.getroot()
        news_list = xml_root.findall("channel/item")

        for one_news in news_list:
            words = one_news.find("description").text.lower().strip().split()
            for one_word in words:
                if len(one_word) >= min_word_lenght:
                    filtered_words.append(one_word)
        return filtered_words
    else:
        return -1


def calculate_words_frequency(filtered_words):
    frequency_words = dict()
    if isinstance(filtered_words, list) and len(filtered_words) != 0:
        for one_word in filtered_words:
            if one_word not in frequency_words.keys():
                frequency_words[one_word] = 1
            else:
                frequency_words[one_word] += 1
        frequency_words_desc = dict(sorted(frequency_words.items(), key=lambda x: x[1], reverse=True))
        return frequency_words_desc
    else:
        return -1


def display_top_n_words(frequency_words, n):
    if isinstance(frequency_words, dict) and len(frequency_words) != 0:
        for num, value in enumerate(frequency_words.items()):
            if num > n - 1:
                break
            else:
                print(f'{num + 1}. Слово "{value[0]}", количество повторений - {value[1]}.')



if __name__ == "__main__":
    XML_FILEPATH = "files/newsafr.xml"
    JSON_FILEPATH = "files/newsafr.json"

    json_words = get_filtered_words(JSON_FILEPATH, "json", 6)
    xml_words = get_filtered_words(XML_FILEPATH, "xml", 6)

    json_frequency = calculate_words_frequency(json_words)
    xml_frequency = calculate_words_frequency(xml_words)

    print(f"Топ слов в новостях из json-файла.")
    display_top_n_words(json_frequency, 10)
    print(f"=================")
    print(f"Топ слов в новостях из xml-файла.")
    display_top_n_words(xml_frequency, 10)
