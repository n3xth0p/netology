import os
from collections import OrderedDict


def read_all_files(list_of_files):
    num_of_strings = {}
    for current_file in list_of_files:
        with open(current_file, "r", encoding="utf-8") as file:
            lenght = len(file.readlines())
            if lenght not in num_of_strings.keys():
                num_of_strings[lenght] = list()
                num_of_strings[lenght].append(current_file)
            else:
                num_of_strings[lenght].append(current_file)

    return num_of_strings


if __name__ == "__main__":
    directory = "sorted/"
    list_of_files = []

    for file in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, file)):
            list_of_files.append(directory + file)

    dictinary_of_files = read_all_files(list_of_files)
    sorted_dictinary_of_files = OrderedDict(sorted(dictinary_of_files.items()))

    for lenght, files in sorted_dictinary_of_files.items():
        for file in files:
            with open(file, "r", encoding="utf-8") as current_file:
                current_data = str(file).replace(directory, "") + "\n" + str(lenght) + "\n" + str(
                    current_file.read()) + "\n"

            with open("result.txt", "a", encoding="utf-8") as result_file:
                result_file.write(current_data)
