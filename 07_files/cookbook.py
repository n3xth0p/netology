#task 1
def parse_recipe_string(input_string):
    if '|' in input_string:
        product_description = input_string.split('|')
        return {'ingridient_name': product_description[0].strip(), 'quantity': int(product_description[1]),
                'measure': product_description[2].strip()}
    elif input_string.isnumeric():
        return {'number': int(input_string)}
    elif '|' not in input_string and input_string != "":
        return {'name': input_string}
    else:
        return {}

#task 2
def get_shop_list_by_dishes(dishes, person_count, cook_book):
    sum_of_product = {}
    for dish in dishes:
        if dish in cook_book.keys():
            for ingridient in cook_book[dish]:
                if ingridient['ingridient_name'] not in sum_of_product.keys():
                    sum_of_product[ingridient['ingridient_name']] = {'measure': ingridient['measure'],
                                                                     'quantity': int(
                                                                         ingridient['quantity'] * person_count)}
                else:
                    sum_of_product[ingridient['ingridient_name']]['quantity'] += int(
                        ingridient['quantity']) * person_count
    return sum_of_product


if __name__ == "__main__":

    cook_book = {}

    with open("recipes.txt", "r", encoding="utf-8") as recipes_file:
        current_read_counter = 0

        for current_line in recipes_file:
            parsed_line_dict = parse_recipe_string(current_line.strip())

            if 'name' in parsed_line_dict.keys() and parsed_line_dict['name'] not in cook_book.keys():
                current_dish_name = parsed_line_dict['name']
                cook_book[current_dish_name] = []

            if 'number' in parsed_line_dict.keys():
                while (current_read_counter < parsed_line_dict['number']):
                    cook_book[current_dish_name].append(parse_recipe_string(recipes_file.readline().strip()))
                    current_read_counter += 1
                current_read_counter = 0

    dishes = ['Омлет', 'миниомлет', 'Фахитос']
    person_count = 2

    print(cook_book)
    print(get_shop_list_by_dishes(dishes, person_count, cook_book))
