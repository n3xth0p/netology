import re
import datetime as dt


class ParserData:
    def __init__(self, type):
        self.type = type

    def parse_albums(self, data=None):
        result = {}
        if self.type == 'album' and data is not None and data['count'] != 0:
            for item in data['items']:
                if item['id'] not in result.keys():
                    result[item['id']] = re.sub("[ \\ \'\*\"\./ ]", '_', item['title'])
            return result
        else:
            return []

    def parse_photos(self, data=None):
        result = []
        if self.type == 'photo' and data is not None and data['count'] != 0:
            for item in data['items']:
                temp_dict = {'id': item['id'], 'likes': item['likes']['count'],
                             'date': dt.datetime.fromtimestamp(item['date']).strftime('%Y-%m-%d_%H_%M_%S'),
                             'url': max(item['sizes'], key=lambda x: x['type'])['url'],
                             'size': max(item['sizes'], key=lambda x: x['type'])['type'],
                             'album_id': item['album_id']}
                result.append(temp_dict)
            return result
        else:
            return []
