import os
import requests
import json
import display as ds


class YaUploader:
    def __init__(self, token: str):
        self.token = token
        self.HEADERS = {
            "Authorization": f"OAuth {self.token}"
        }
        self.URL = "https://cloud-api.yandex.net/v1/disk/resources"

    def upload(self, local_result_file: str, upload_directory='vk_upload'):

        if os.path.exists(local_result_file):
            with open(local_result_file, "r", encoding="utf-8") as f:
                files_to_upload = json.load(f)
        else:
            raise Exception

        if not self.__exists(upload_directory):
            self.create_dir(upload_directory)
        else:
            print(f"директория {upload_directory} уже есть на диске. укажите другую.")
            raise Exception

        uploaded_files_result = []
        l = len(files_to_upload)

        counter = 0
        print(f"Uploading photo to yandex directory: {upload_directory}")
        for file in files_to_upload:
            file_directory = f"{file['path']}".split("/")

            if len(file_directory) == 2 and not self.__exists(f"{upload_directory}/{file_directory[1]}"):
                self.create_dir(f"{upload_directory}/{file_directory[1]}")

            result = self.__upload_file(f"{file['path']}/{file['file_name']}",
                                        f"{upload_directory}/{file_directory[1]}/{file['file_name']}")
            uploaded_files_result.append({f"{file['file_name']}": result})

            counter += 1
            ds.show_progress_bar(counter, l, prefix='Progress:', suffix='Complete', length=50)

        return result

    def __upload_file(self, local_file_path, upload_filepath):
        # шаг 1. получение URL для загрузки файла
        i = 1

        while (True):
            response = requests.get(
                f"{self.URL}/upload",
                params={
                    "path": upload_filepath
                },
                headers=self.HEADERS
            )

            if 'href' in response.json().keys():
                href = response.json()["href"]
                break

            else:
                print(response.text)
                print(f"next try to get href for file: {local_file_path}")

            i += 1

            if i > 3:
                return response.status_code

        # шаг 2. загрузка тела файла
        with open(local_file_path, "rb") as f:
            upload_response = requests.put(href, files={"file": f})
            upload_response.raise_for_status()

        return upload_response.status_code

    def __exists(self, name: str):
        response = requests.get(
            self.URL,
            params={
                "path": name
            },
            headers=self.HEADERS
        )

        if response.status_code == 404:
            return False
        elif response.status_code == 200:
            return True
        else:
            raise Exception

    def create_dir(self, path):
        response = requests.put(
            self.URL,
            params={
                "path": path,
                "templated": "true"
            },
            headers=self.HEADERS
        )
        if response.status_code == 201:
            return True
        else:
            raise Exception
