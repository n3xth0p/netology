import vkuser as vk
import ya_disk as ya
import pprint as pp

if __name__ == "__main__":
    # add oauth token to vk
    with open('..\\..\\mytoken.txt', 'r') as file_object:
        token = file_object.read().strip()

    # add some vk users
    API_VERSION = '5.126'
    DIRECTORY = "vk_upload"

    vk_user_id = int(input("Введите id профиля Вконтакте: "))
    yandex_token = str(input("Введите oath-токен яндекс-диска: "))

    user_profile = vk.VkUser(token, API_VERSION, vk_user_id)
    uploader = ya.YaUploader(yandex_token)

    command = ""
    while command.lower() not in ['1', '2', 'q']:
        if command == "":
            command = input(f"1 - скачать фото профиля \n"
                            f"2 - скачать все фотоальбомы \n"
                            f"q - выход\n"
                            f"введите команду: ")
        else:
            command = input(f" неверный ввод, введите команду: ")

    if command == '1':
        print(f"Приступаю к скачиваю фото профиля в локальную папку {DIRECTORY}")
        user_profile.download_photos(user_profile.get_photos(), DIRECTORY, 1000)
        print(f"Приступаю к загрузке фото профиля в на яндекс диск в папку {DIRECTORY}")
        uploader.upload(f"{DIRECTORY}/result.json", DIRECTORY)
    elif command == '2':
        print(f"Приступаю к скачиваю фото альбомов в локальную папку {DIRECTORY}")
        user_profile.download_photos(user_profile.get_photos_all(user_profile.get_photo_albums()), DIRECTORY, 10_000)
        print(f"Приступаю к загрузке фото альбомов в на яндекс диск в папку {DIRECTORY}")
        uploader.upload(f"{DIRECTORY}/result.json", DIRECTORY)
    else:
        pass
