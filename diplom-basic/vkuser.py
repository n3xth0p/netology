import parserdata as prs
import requests
import time
import os
import json
import display as ds


class VkUser:
    url = 'https://api.vk.com/method/'

    def __init__(self, token, version, profile_id=None):
        self.token = token
        self.version = version
        self.params = {
            'access_token': self.token,
            'v': self.version
        }
        if profile_id is None:
            self.profile_id = requests.get(self.url + 'users.get', self.params).json()['response'][0]['id']
        else:
            self.profile_id = profile_id

    def __str__(self):
        return f"https://vk.com/id{self.profile_id}"

    def get_photo_albums(self, target_uid=None):
        if target_uid is None:
            target_uid = self.profile_id
        albums_url = self.url + 'photos.getAlbums'
        albums_params = {
            'owner_id': target_uid,
        }
        res = requests.get(albums_url, params={**self.params, **albums_params})
        my_parser = prs.ParserData('album')
        result = my_parser.parse_albums(res.json()['response'])
        return result

    def get_photos(self, album_id='profile', target_uid=None):
        if target_uid is None:
            target_uid = self.profile_id
        photo_url = self.url + 'photos.get'
        photo_params = {
            'owner_id': target_uid,
            'album_id': album_id,
            'extended': 1,
            'count': 1000
        }
        res = requests.get(photo_url, params={**self.params, **photo_params})
        res.raise_for_status()
        my_parser = prs.ParserData('photo')
        result = my_parser.parse_photos(res.json()['response'])
        return result

    def get_photos_all(self, albums, target_uid=None):
        if target_uid is None:
            target_uid = self.profile_id
        result = []
        count = 0
        for album_id in albums.keys():
            temp_photos = self.get_photos(album_id, target_uid)

            for one_photo in temp_photos:
                result.append(one_photo)

            time.sleep(0.4)  # pause between requests for VK API
            count += 1
            print(f"Подождите запрашиваю фото с альбома ... {count}")
        return result

    def download_photos(self, photos_list, directory="download", number=5):
        downloaded_files = []
        # print("this is photo" + str(photos_list))

        if len(photos_list) != 0:
            if not os.path.exists(directory):
                os.makedirs(directory)
            counter = 0

            if number <= len(photos_list):
                l = number
            else:
                l = len(photos_list)
            print(f"Downloading photo to local directory: {directory}")

            for photo in photos_list:
                if counter >= number:
                    break

                response = requests.get(photo['url'])

                if response.status_code == 200:
                    content = response.content

                    temp_dict = {'file_name': f"{photo['likes']}_{photo['date']}_{photo['id']}.jpg",
                                 'size': photo['size'],
                                 'path': f"{directory}/{photo['album_id']}"
                                 }

                    temp_filename = f"{directory}/{photo['album_id']}/{temp_dict['file_name']}"

                    if not os.path.exists(f"{directory}/{photo['album_id']}"):
                        os.makedirs(f"{directory}/{photo['album_id']}")

                    with open(temp_filename, "wb") as f:
                        f.write(content)

                    temp_dict["file_name"] = f"{photo['likes']}_{photo['date']}_{photo['id']}.jpg"
                    downloaded_files.append(temp_dict)

                counter += 1
                ds.show_progress_bar(counter, l, prefix='Progress:', suffix='Complete', length=50)

            with open(f"{directory}/result.json", "w", encoding="utf-8") as dict_file:
                json.dump(downloaded_files, dict_file, ensure_ascii=False, indent=2)

        return downloaded_files
