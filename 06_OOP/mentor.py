import student as st

class Mentor:
    def __init__(self, name, surname):
        self.name = name
        self.surname = surname
        self.courses_attached = []

    def __str__(self):
        return f"Имя: {self.name}\n" \
               f"Фамилия: {self.surname}\n"

class Lecturer(Mentor):
    def __init__(self, name, surname):
        super().__init__(name, surname)
        self.grades = {}

    def __average_grades(self):
        full_grades = []
        for course, grade in self.grades.items():
            full_grades += grade

        if len(full_grades) == 0:
             return 0
        else:
             return round(sum(full_grades) / len(full_grades), 2)

    def __str__(self):
        return f"Имя: {self.name}\n" \
               f"Фамилия: {self.surname}\n" \
               f"Средняя оценка за лекции: {self.__average_grades()}\n"

    def __le__(self, other):
         if self.__average_grades() <= other.__average_grades():
             return True
         else:
             return False

    def __ge__(self, other):
         if self.__average_grades() >= other.__average_grades():
             return True
         else:
             return False

    def __lt__(self, other):
         if self.__average_grades() < other.__average_grades():
             return True
         else:
             return False

    def __gt__(self, other):
         if self.__average_grades() > other.__average_grades():
             return True
         else:
             return False

    def __eq__(self, other):
         if self.__average_grades() == other.__average_grades():
             return True
         else:
             return False

    def __ne__(self, other):
         if self.__average_grades() != other.__average_grades():
             return True
         else:
             return False

    # лучше сделать как метод класса (на будущее)
    def avg_lectors_course_grades(self, lectors_list, course_name):
        course_grades = []
        for one_lector in lectors_list:
            if isinstance(one_lector, Lecturer) and (course_name in one_lector.courses_attached):
                for course, grade in one_lector.grades.items():
                    course_grades += grade

        if len(course_grades) == 0:
            return 0
        else:
            message = f"Средняя оценка по лекторам за курс {course_name} равна {round(sum(course_grades) / len(course_grades), 2)}"
            return message


class Reviewer(Mentor):

    def rate_howework(self, student, course, grade):
        if isinstance(student, st.Student) and course in student.courses_in_progress:
            if course in student.grades:
                student.grades[course] += [grade]
            else:
                student.grades[course] = [grade]
        else:
            return 'Ошибка'
