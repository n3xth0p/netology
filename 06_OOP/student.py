import mentor as mt

class Student:
    def __init__(self, name, surname, gender):
        self.name = name
        self.surname = surname
        self.gender = gender
        self.finished_courses = []
        self.courses_in_progress = []
        self.grades = {}

    def add_courses(self, course_name):
        self.finished_courses.append(course_name)

    def __average_grades(self):
        full_grades = []
        for course, grade in self.grades.items():
            full_grades += grade

        if len(full_grades) == 0:
            return 0
        else:
            return round(sum(full_grades) / len(full_grades), 2)

    def __str__(self):
        student_info = f"Имя: {self.name}\nФамилия: {self.surname}\n" \
                  f"Средняя оценка за домашние задания: {self.__average_grades()}\n" \
                  f"Курсы в процессе изучения:  { self.courses_in_progress.__str__() }\n" \
                  f"Завершенные курсы: { self.finished_courses.__str__() }\n"
        return student_info

    def rate_lecture(self, lector, course, grade):
        if isinstance(lector, mt.Lecturer) and course in lector.courses_attached and course in self.courses_in_progress:
            if course in lector.grades:
                lector.grades[course] += [grade]
            else:
                lector.grades[course] = [grade]
        else:
            return 'Ошибка'

    def __le__(self, other):
         if self.__average_grades() <= other.__average_grades():
             return True
         else:
             return False

    def __ge__(self, other):
         if self.__average_grades() >= other.__average_grades():
             return True
         else:
             return False

    def __lt__(self, other):
         if self.__average_grades() < other.__average_grades():
             return True
         else:
             return False

    def __gt__(self, other):
         if self.__average_grades() > other.__average_grades():
             return True
         else:
             return False

    def __eq__(self, other):
         if self.__average_grades() == other.__average_grades():
             return True
         else:
             return False

    def __ne__(self, other):
         if self.__average_grades() != other.__average_grades():
             return True
         else:
             return False

    # лучше сделать как метод класса (на будущее)
    def avg_students_course_grades(self, student_list, course_name):
        course_grades = []
        for one_student in student_list:
            if isinstance(one_student, Student) and (course_name in one_student.courses_in_progress or course_name in one_student.finished_courses):
                for course, grade in one_student.grades.items():
                    course_grades += grade

        if len(course_grades) == 0:
            return 0
        else:
            message = f"Средняя оценка по студентам за курс {course_name} равна {round(sum(course_grades) / len(course_grades), 2)}"
            return message
