import mentor as mt
import student as st

def avg_lectors_course_grades (lectors, course):
    pass

def avg_students_course_grades (lectors, course):
    pass

if __name__ == "__main__":
    student_01 = st.Student('Ruoy', 'Eman', 'male')
    student_01.courses_in_progress += ['Python']
    student_01.courses_in_progress += ['Git']
    student_01.courses_in_progress += ['Machine_Learning']

    student_02 = st.Student('Jane', 'Doe', 'female')
    student_02.courses_in_progress += ['Machine_Learning']
    student_02.courses_in_progress += ['Python']

    cool_lector_01 = mt.Lecturer('Doc', 'Sapolsky')
    cool_lector_01.courses_attached += ['Python']
    cool_lector_01.courses_attached += ['Git']

    cool_lector_02 = mt.Lecturer('Some', 'Buddy')
    cool_lector_02.courses_attached += ['Machine_Learning']

    cool_reviewer_01 = mt.Reviewer('Evil', 'Reviewerer')
    cool_reviewer_02 = mt.Reviewer('Good', 'Reviewerer')

    cool_reviewer_01.rate_howework(student_01,'Git',10)
    cool_reviewer_01.rate_howework(student_01, 'Git', 3)

    cool_reviewer_01.rate_howework(student_01, 'Python', 7)
    cool_reviewer_01.rate_howework(student_01, 'Python', 2)
    cool_reviewer_01.rate_howework(student_01, 'Python', 4)
    cool_reviewer_01.rate_howework(student_01, 'Python', 1)
    cool_reviewer_02.rate_howework(student_01, 'Python', 7)
    cool_reviewer_02.rate_howework(student_02, 'Python', 9)

    student_01.add_courses('Python')

    cool_reviewer_01.rate_howework(student_02, 'Machine_Learning', 2)
    cool_reviewer_01.rate_howework(student_02, 'Machine_Learning', 3)

    student_01.rate_lecture(cool_lector_01,'Cooking',10)
    student_01.rate_lecture(cool_lector_01, 'Python', 8)
    student_01.rate_lecture(cool_lector_01, 'Git', 9)

    student_02.rate_lecture(cool_lector_01, 'Git', 2)
    student_02.rate_lecture(cool_lector_01, 'Python', 5)
    student_02.rate_lecture(cool_lector_02, 'Machine_Learning', 10)

    print("===Студенты===")
    print(student_01)
    print(student_02)
    print(f"сравниваем средние оценки student_01 > student_02 {student_01 > student_02}")
    print(f"сравниваем средние оценки student_01 >= student_02 {student_01 >= student_02}")
    print(f"сравниваем средние оценки student_01 <= student_02 {student_01 <= student_02}")
    print(f"сравниваем средние оценки student_01 < student_02 {student_01 < student_02}")
    print(f"сравниваем средние оценки student_01 == student_02 {student_01 == student_02}")
    print(f"сравниваем средние оценки student_01 != student_02 {student_01 != student_02}")

    print("\n===Лекторы===")
    print(cool_lector_01)
    print(cool_lector_02)

    print(f"сравниваем средние оценки cool_lector_01 > cool_lector_02 {cool_lector_01 > cool_lector_02}")
    print(f"сравниваем средние оценки cool_lector_01 >= cool_lector_02 {cool_lector_01 >= cool_lector_02}")
    print(f"сравниваем средние оценки cool_lector_01 <= cool_lector_02 {cool_lector_01 <= cool_lector_02}")
    print(f"сравниваем средние оценки cool_lector_01 < cool_lector_02 {cool_lector_01 < cool_lector_02}")
    print(f"сравниваем средние оценки cool_lector_01 == cool_lector_02 {cool_lector_01 == cool_lector_02}")
    print(f"сравниваем средние оценки cool_lector_01 != cool_lector_02 {cool_lector_01 != cool_lector_02}")



    print("\n===Оценщики===")
    print(cool_reviewer_01)
    print(cool_reviewer_02)

    students = [student_01, student_02]
    lectors = [cool_lector_01, cool_lector_02]
    print(student_01.avg_students_course_grades(students,'Python'))
    print(student_01.avg_students_course_grades(students, 'Git'))
    print(student_01.avg_students_course_grades(students, 'Machine_Learning'))

    print(cool_lector_01.avg_lectors_course_grades(lectors,'Python'))
    print(cool_lector_01.avg_lectors_course_grades(lectors, 'Git'))
    print(cool_lector_01.avg_lectors_course_grades(lectors, 'Machine_Learning'))
