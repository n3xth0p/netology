import requests


class VkUser:
    url = 'https://api.vk.com/method/'

    def __init__(self, token, version, profile_id=None):
        self.token = token
        self.version = version
        self.params = {
            'access_token': self.token,
            'v': self.version
        }
        if profile_id is None:
            self.profile_id = requests.get(self.url + 'users.get', self.params).json()['response'][0]['id']
        else:
            self.profile_id = profile_id

    def get_mutual_friends(self, target_uid, source_uid=None):
        if source_uid is None:
            source_uid = self.profile_id
        friends_url = self.url + 'friends.getMutual'
        friends_params = {
            'count': 1000,
            'source_uid': source_uid,
            'target_uid': target_uid
        }
        res = requests.get(friends_url, params={**self.params, **friends_params})
        return res.json()['response']

    def __str__(self):
        return f"https://vk.com/id{self.profile_id}"

    def __and__(self, other):
        friends_list = []
        for friend in self.get_mutual_friends(other.profile_id):
            temp_friend = VkUser(token, '5.126', friend)
            friends_list.append(temp_friend)
        return friends_list


if __name__ == "__main__":
    # add own token
    with open('..\\..\\mytoken.txt', 'r') as file_object:
        token = file_object.read().strip()

    # add some vk users
    FRIENDS_PROFILE_ID_01 = ""
    FRIENDS_PROFILE_ID_02 = ""
    NOT_FRIENDS_PROFILE_01 = ""
    NOT_FRIENDS_PROFILE_02 = ""

    my_profile = VkUser(token, '5.126')
    print(my_profile.get_mutual_friends(FRIENDS_PROFILE_ID_01))
    print(my_profile.get_mutual_friends(FRIENDS_PROFILE_ID_01, FRIENDS_PROFILE_ID_02))
    print(my_profile.get_mutual_friends(FRIENDS_PROFILE_ID_01, FRIENDS_PROFILE_ID_02))
    print(my_profile.get_mutual_friends(NOT_FRIENDS_PROFILE_01, NOT_FRIENDS_PROFILE_02))

    friends_profile = VkUser(token, '5.126', FRIENDS_PROFILE_ID_01)

    mutual_friends_class_list = my_profile & friends_profile

    for friend in mutual_friends_class_list:
        print(isinstance(friend, VkUser), friend)
